---
author:
  name: Zhongjie Shen
  email: shenzj1994@gmail.com
description: 'Running Multiple Wordpress Sites on One Linode.'
keywords: 'WordPress, MySQL, Apache2, PHP'
license: '[CC BY-ND 4.0](https://creativecommons.org/licenses/by-nd/4.0)'
published: 'Weekday, Month 00st, 2017'
modified: Weekday, Month 00th, 2017
modified_by:
  name: Linode
title: 'Running Multiple Wordpress Sites on One Linode.'
contributor:
  name: Your Name
  link: Github/Twitter Link
  external_resources:
- '[Official WordPress Website](https://wordpress.org)'
---
*This is a Linode Community guide. If you're an expert on something we need a guide on, you too can [get paid to write for us](/docs/contribute).*
----

## Introduction
This guide will help you install more than one WordPress sites on a single VPS or dedicated server.

## Before you Begin
1. Get familiar with WordPress. '[Official WordPress Website](https://wordpress.org)' is a good start point. It also describes the steps to install a single WordPress site which can be used as reference as well.
2. This guide will use `sudo` wherever possible. Make sure you have the previllage to execute `sudo`.
3. Update your system:

		sudo apt-get update && sudo apt-get upgrade
4. Install the following system utilities we will use in this guide:
	* nano
	* wget
	* unzip
	

{: .note}
>
> This guide is written for a non-root user. Commands that require elevated privileges are prefixed with `sudo`. If you’re not familiar with the `sudo` command, see the [Users and Groups](/docs/tools-reference/linux-users-and-groups) guide.
> Replace each instance of `example*.com` in this guide with your site's domain name.
> This guide is based on Apache2+PHP+MySQL environment. If you have a different environment, steps and commands below may or may not apply to you. Please adjust these commands based on your needs.


## Download WordPress Archive

1. Create a folder for the archive. Use the following command:
	
	Make a directory for download file at your home directory:

		cd ~		
		mkdir WP_DL
		
2. Use wget to download the archive: (This is the default address for the latest version of WordPress. If the address changes in the future, you can always find it by visiting '[Official WordPress Website](https://wordpress.org)'.
		
		cd WP_DL
		wget https://wordpress.org/latest.zip
		
## Make Site Folders
1. Depends on the number of WordPress sites you want to install. You can create these site folders by using:

		sudo mkdir /var/www/example1.com
		sudo mkdir /var/www/example2.com
		sudo mkdir /var/www/example3.com
		...
2. Extract the WordPress files to each site folder

		sudo unzip latest.zip -d /var/www/example1.com
		sudo unzip latest.zip -d /var/www/example2.com
		sudo unzip latest.zip -d /var/www/example3.com
		...
3. Change the owner if these site folders

		sudo chown -R www-data:www-data /var/www/example1.com
		sudo chown -R www-data:www-data /var/www/example2.com
		sudo chown -R www-data:www-data /var/www/example3.com
		...

## Create Configuration Files
*To be continuted*
